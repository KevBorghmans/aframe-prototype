function loadDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {

    if (this.readyState == 4 && this.status == 200) {
      var obj = JSON.parse(this.responseText);
      // document.getElementById("demo").innerHTML = "value : " + obj.total_value_art + ", visitors: " + obj.total_number_visitors;
        var sceneEl = document.querySelector('a-scene');
        var count = sceneEl.querySelector('#count');
        count.setAttribute('text', {
        value: '€' + obj.total_number_visitors
      });
    }
  };

  xhttp.open("GET", "https://kasboek.org/kassa/kassa.php?action=get_current", true);
  xhttp.send();
}
loadDoc();

setInterval(loadDoc, 3000);