var myTimeout;

AFRAME.registerComponent('block-teleport-1', {

  init: function () {
    var data = this.data;
    var el = this.el; // <a-box>
    var cam = sceneEl.querySelector('#cam');

    el.addEventListener('mouseenter', function () {
      myTimeout = setTimeout(function () {
        cam.setAttribute('position', {
          x: 7,
          y: 0,
          z: -15
        });
      }, 1500);
    });

    el.addEventListener('mouseleave', function () {
      clearTimeout(myTimeout);
    });
  }
});

AFRAME.registerComponent('block-teleport-2', {

  init: function () {
    var data = this.data;
    var el = this.el; // <a-box>
    var cam = sceneEl.querySelector('#cam');

    el.addEventListener('mouseenter', function () {
      myTimeout = setTimeout(function () {
        cam.setAttribute('position', {
          x: 10,
          y: 0,
          z: 0
        });
      }, 1500);
    });

    el.addEventListener('mouseleave', function () {
      clearTimeout(myTimeout);
    });
  }
});
    AFRAME.registerComponent('block-teleport-3', {

      init: function () {
        var data = this.data;
        var el = this.el; // <a-box>
        var cam = sceneEl.querySelector('#cam');

        el.addEventListener('mouseenter', function () {
          myTimeout = setTimeout(function () {
            cam.setAttribute('position', {
              x: -5,
              y: 0,
              z: 0
            });
          }, 1500);
        });

        el.addEventListener('mouseleave', function () {
          clearTimeout(myTimeout);
        });
      }
    });

    AFRAME.registerComponent('block-teleport-4', {

      init: function () {
        var data = this.data;
        var el = this.el; // <a-box>
        var cam = sceneEl.querySelector('#cam');

        el.addEventListener('mouseenter', function () {
          myTimeout = setTimeout(function () {
            cam.setAttribute('position', {
              x: -1.6,
              y: 0,
              z: -7.2
            });
          }, 1500);
        });

        el.addEventListener('mouseleave', function () {
          clearTimeout(myTimeout);
        });
      }
    });

    AFRAME.registerComponent('block-teleport-5', {

      init: function () {
        var data = this.data;
        var el = this.el; // <a-box>
        var cam = sceneEl.querySelector('#cam');

        el.addEventListener('mouseenter', function () {
          myTimeout = setTimeout(function () {
            cam.setAttribute('position', {
              x: -5,
              y: 0,
              z: -15
            });
          }, 1500);
        });

        el.addEventListener('mouseleave', function () {
          clearTimeout(myTimeout);
        });
      }
    });