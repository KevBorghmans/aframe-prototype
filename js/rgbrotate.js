var r = 255,
      g = 0,
      b = 0;

    setInterval(function () {
      if (r > 0 && b == 0) {
        r--;
        g++;
      }
      if (g > 0 && r == 0) {
        g--;
        b++;
      }
      if (b > 0 && g == 0) {
        r++;
        b--;
      }
      var color = "#" + hex(r) + hex(g) + hex(b);

      function hex(v) {

        var hex = v.toString(16);
        if (v < 16) {
          hex = "0" + hex;
        }
        return hex;
      }
      var sceneEl = document.querySelector('a-scene');
      var entity = sceneEl.querySelector('#ambient');
      var ent = sceneEl.querySelector('#colorbox');

      entity.setAttribute('light', {
        color: color,
        intensity: 1
      }); 
    }, 50);